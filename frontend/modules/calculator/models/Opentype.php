<?php

namespace frontend\modules\calculator\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "opentype".
 *
 * @property integer $id
 * @property string $type
 * @property integer $price
 */
class Opentype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opentype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'price'], 'required'],
            [['id', 'price'], 'integer'],
            [['type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'price' => 'Price',
        ];
    }
    
    public function findModel($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function findName($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model->type;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
   
}
