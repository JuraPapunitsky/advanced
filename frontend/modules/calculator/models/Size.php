<?php

namespace frontend\modules\calculator\models;

use Yii;

/**
 * This is the model class for table "size".
 *
 * @property integer $id
 * @property integer $sqare
 */
class Size extends \yii\db\ActiveRecord
{
    
    public $height = 165;
    
    public $weight = 150;
    
    public $price;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'size';
    }
    
    public function setDefaultSettings()
    {
        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sqare', 'height', 'weight', 'price'], 'required'],
            [['sqare', 'height', 'weight'], 'integer'],
        ];
    }
    
    public function setPrice($price)
    {
        $this->price = ($this->height/10)*($this->weight/10)*$price;
    }
    
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sqare' => 'Sqare',
            'height' => 'Высота см',
            'weight' => 'Ширина см',
            'price' => 'Цена',
        ];
    }
    
    public function findModel($id)
    {
        if (($model = Size::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
