<?php

namespace frontend\modules\calculator\models;

use Yii;
use frontend\modules\calculator\models\Opentype;

/**
 * This is the model class for table "casements".
 *
 * @property integer $id
 * @property integer $oneflap
 * @property integer $twoflap
 * @property integer $treeflap
 * @property integer $framula
 */
class Casements extends \yii\db\ActiveRecord
{
    
    public $flap = '1';
    
    public $isFramula = false;
    
    public $price;
    
    
    
    public $typeone = 1;
    public $typetwo = 2;
    public $typethree = 1;
    
    
    

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'casements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oneflap', 'twoflap', 'treeflap', 'framula', 'flap', 'price', 'isFramula', 'typeone', 'typetwo', 'typethree',], 'required'],
            [['oneflap', 'twoflap', 'treeflap', 'framula'], 'integer'],
            [['isFramula',], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oneflap' => 'Oneflap',
            'twoflap' => 'Twoflap',
            'treeflap' => 'Treeflap',
            'framula' => 'Framula',
            'flap' => 'Количество створок',
            'isFramula' => 'Фрамуга (форточка) ' ,
            'price' => 'Цена',
            'typeone' => 'Тип первой створки',
            'typetwo' => 'Тип второй створки',
            'typethree' => 'Тип третей створки',
        ];
    }
    
    public function getFramulaPrice()
    {
        return $this->findModel(1)->framula;
    }
    
    public function getFlapPrice()
    {
        
        if($this->flap == 1)
        {
            return $this->findModel(1)->twoflap;
        }  
        else if ($this->flap == 2)
        {
            return $this->findModel(1)->treeflap;
        }else
        {
            return 0;
        }
        
        
    }

    
    public function setPrice($price)
    {
        if (!$this->isFramula == 1){
        $this->price = $price+$this->getFlapPrice();
        }  else {
            $this->price = $price+$this->getFlapPrice()+$this->getFramulaPrice();    
        }
        
        $this->price = $this->price+$this->getOpenTypePrice($this->typeone)+
               $this->getOpenTypePrice($this->typetwo)+
               $this->getOpenTypePrice($this->typethree);
                
    }
    
    public function findModel($id)
    {
        if (($model = Casements::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public function getOpenTypePrice($id)
    {
        if (($model = Opentype::findOne($id)) !== null) {
            return $model->price;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
   
}
