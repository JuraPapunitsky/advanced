<?php

namespace frontend\modules\calculator\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $order
 * @property string $client_name
 * @property string $email
 * @property string $phone
 * @property string $date
 */
class Order extends \yii\db\ActiveRecord
{
    public $height;
    
    public $weight;
    
    
    
    public $flap;
    
    public $isFramula;
    
    
    public $typeone;
    public $typetwo;
    public $typethree;
    
    public $series;
    public $glazing;
    public $price;
    
    
    public $order = '';
    
    public $orderSave;

    public $verifyCode;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order', 'orderSave'], 'string'],
            [['client_name', 'phone', 'email'], 'required'],
            [['date'], 'safe'],
            [['client_name', 'email', 'phone'], 'string', 'max' => 255],
            [['email',],'email'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderSave' => 'Ваш заказ',
            'client_name' => 'Ваше имя',
            'email' => 'Ваш Email',
            'phone' => 'Ваш Телефон',
            
            
        ];
    }
    
    public function setOrder($size, $casements, $pane)
    {
        
        $this->order .= 'Окно размером '.$size->height.' * '.$size->weight.'.';
        if($casements->flap == '1')
        {
            $this->order .= ' 2 створки'.$casements->typeone.$casements->typetwo;
        }else if($casements->flap == '2')
        {
            $this->order .= ' 3 створки'.$casements->typeone.$casements->typetwo.$casements->typethree;
        }
        
        if($casements->isFramula == 1)
        {
            $this->order .= '+фрамуга';
        }
        
        $this->order .= ' Профиль '.$pane->series.' стеклопакет '. $pane->glazing.' Цена '.$pane->price;
        
        $this->orderSave = $this->order;
        
    }
}
