<?php

namespace frontend\modules\calculator\models;

class Pane extends \yii\base\Model{
    
    public $series = 1;
    
    public $glazing = 1;
    
    public $price;


    public function rules()
    {
        return[
            
            [['series','glazing', 'price'], 'required'],
            [['series','glazing'], 'integer'],
            
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'series'=>'Профиль',
            'glazing'=>'Стеклопакет',
            'price' => 'Цена',
        ];
    }
    
    public function setPrice($price)
    {
        $this->price = $price+$this->getSeriesPrice($this->series)+
                $this->getGlazingPrice($this->glazing);
    }
    
    
    public function getSeriesPrice($id)
    {
         if (($model = Series::findOne($id)) !== null) {
            return $model->price;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function getGlazingPrice($id)
    {
         if (($model = Glazing::findOne($id)) !== null) {
            return $model->price;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }





    
    
    
}
