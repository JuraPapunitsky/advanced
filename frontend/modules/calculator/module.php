<?php

namespace frontend\modules\calculator;

class module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\calculator\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
