<?php

namespace frontend\modules\calculator\controllers;

use yii\web\Controller;
use frontend\modules\calculator\models\Size;
use frontend\modules\calculator\models\Casements;
use frontend\modules\calculator\models\Opentype;
use frontend\modules\calculator\models\Pane;
use frontend\modules\calculator\models\Series;
use frontend\modules\calculator\models\Glazing;
use frontend\modules\calculator\models\Order;
use Yii;

class DefaultController extends Controller
{
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSize()
    {
        $model = new Size();
        
        $session = Yii::$app->session;
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }
        $sqare = $model->findModel(1);
        $model->setPrice($sqare->sqare);
        
        $session->set('size', $model);
        
        return $this->render('size', [
            'model' => $model,
        ]);
    }
    
    public function actionCasements()
    {
        $model = new Casements();
        $queryOpentype = Opentype::find();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                
                // form inputs are valid, do something here
                return;
            }
        }
        
        $session = Yii::$app->session;
        
        $size = $session->get('size');
        
        $opentypes = $queryOpentype->orderBy('id')->all();
                
        $model->setPrice($size->price);
        
        $session->set('casements', $model);
        
        
        return $this->render('casements', [
            'model' => $model,
            'size' => $size,
            'opentypes' => $opentypes,
            
            
        ]);
    }
    
    
    public function actionPane()
    {
        $model = new Pane();
        $seriesQuery = Series::find();
        $glazingQuery = Glazing::find();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                
                
                $session = Yii::$app->session;
        
                $casements = $session->get('casements');

                $model->setPrice($casements->price);

                $series = $seriesQuery->orderBy('id')->all();
                $glazing = $glazingQuery->orderBy('id')->all();
                
                $session->set('pane', $model);
                
                
                
                return $this->render('pane', [
                        'model' => $model,
                        'series'=>$series,
                        'glazing'=>$glazing,
                        //'allnames'=> $allNames,
                    ]);
            }
        }
        
        $session = Yii::$app->session;
        
        $casements = $session->get('casements');
        
        $model->setPrice($casements->price);
        
        $series = $seriesQuery->orderBy('id')->all();
        $glazing = $glazingQuery->orderBy('id')->all();
        
        //$allNames = $model->modelNamesToArray([$series,$glazing]);

        return $this->render('pane', [
            'model' => $model,
            'series'=>$series,
            'glazing'=>$glazing,
            //'allnames'=> $allNames,
        ]);
    }
    
    
    public function actionOrder()
    {
                
        $session = Yii::$app->session;
        
        $size = $session->get('size');
        $casements = $session->get('casements');
        $pane = $session->get('pane');
                        
        $model = new Order();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                
                
                
                $model->setOrder($size, $casements, $pane);
                if($model->save() == true);
                $session->setFlash('success', 'Заказ прийнят');
                return $this->render('order', [
                'model' => $model,
            ]);
            }
        }
         $model->setOrder($size, $casements, $pane);
        
            return $this->render('order', [
                'model' => $model,
            ]);
        
    }
    
    
}
