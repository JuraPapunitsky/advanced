<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model frontend\modules\calculator\models\Order */
/* @var $form ActiveForm */
?>
<div class="order">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'orderSave')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'client_name') ?>
        <?= $form->field($model, 'phone') ?>
        
        <?= $form->field($model, 'email') ?>
    
        
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- order -->
