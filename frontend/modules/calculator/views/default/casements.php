<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model frontend\modules\calculator\models\Casements */
/* @var $form ActiveForm */
?>
<div class="casements">

    <h1>Створки</h1>
    
     <?php 
    $radio = [];
    $i = 1;
        foreach($opentypes as $opentype)
        {
           $radio[$i] = $opentype->type;
           $i++;
        }
        
    ?>
    
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'flap')->radioList(['1' => 'Две створки', '2' => 'Три Створки'],['onChange' => 'hide()']); ?>
        <?= $form->field($model, 'isFramula')->checkbox() ?>
        
    <div>
        <?= $form->field($model, 'typeone')->radioList($radio); ?>
    </div>
    <div>
        <?= $form->field($model, 'typetwo')->radioList($radio); ?>
    </div>
    <div id="ifThree" >
        <?= $form->field($model, 'typethree')->radioList($radio); ?>
    </div>
        <?= $form->field($model, 'price')->label() ?>
    
        
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
   <div>
            <?= Html::a('Профиль и стеклопакет', ['pane'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Заказать', ['order'], ['class' => 'btn btn-success']) ?>
    </div>
    
    
    
   </div><!-- casements -->
