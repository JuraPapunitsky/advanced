<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\calculator\models\Size */
/* @var $form ActiveForm */
?>
<div class="size">
    
    <h1>Замеры</h1>

    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'height')->textInput() ?>
        <?= $form->field($model, 'weight')->textInput() ?>
        <?= $form->field($model, 'price')->label() ?>
    <pre>
        <?php //print_r($model); ?>
    </pre>
        <div class="form-group">
            <?= Html::submitButton('Расчитать', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    <div>Если вам не нужны дополнительные параметры Вы можете заказать или перейти к створкам.</div>
    <div>
            <?= Html::a('Створки', ['casements'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Заказать', ['order'], ['class' => 'btn btn-success']) ?>
    </div>
</div><!-- size -->
