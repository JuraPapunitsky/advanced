<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\calculator\models\Pane */
/* @var $form ActiveForm */
?>
<div class="pane">
    
    <h1>Профиль и стеклопакет</h1>

    <?php $form = ActiveForm::begin(); 
    
$seriesItems = [];
    $i = 1;
        foreach($series as $seria)
        {
           $seriesItems[$i] = $seria->name;
           $i++;
        }
        
$glazingItems = [];
    $i = 1;
        foreach($glazing as $glas)
        {
           $glazingItems[$i] = $glas->name;
           $i++;
        }        
    ?>

        <?= $form->field($model, 'series')->radioList($seriesItems) ?>
        <?= $form->field($model, 'glazing')->radioList($glazingItems) ?>
    
        <?= $form->field($model, 'price')?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

    <div>
            <?= Html::a('Заказать', ['order'], ['class' => 'btn btn-success']) ?>
    </div>
    
</div><!-- pane -->
