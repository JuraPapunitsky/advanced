<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Casements */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="casements-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'oneflap')->textInput() ?>

    <?= $form->field($model, 'twoflap')->textInput() ?>

    <?= $form->field($model, 'treeflap')->textInput() ?>

    <?= $form->field($model, 'framula')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
