<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchCasements */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Casements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="casements-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Casements', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'oneflap',
            'twoflap',
            'treeflap',
            'framula',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
