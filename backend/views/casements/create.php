<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Casements */

$this->title = 'Create Casements';
$this->params['breadcrumbs'][] = ['label' => 'Casements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="casements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
