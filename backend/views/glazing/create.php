<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Glazing */

$this->title = 'Create Glazing';
$this->params['breadcrumbs'][] = ['label' => 'Glazings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="glazing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
