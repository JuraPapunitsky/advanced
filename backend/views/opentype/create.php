<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Opentype */

$this->title = 'Create Opentype';
$this->params['breadcrumbs'][] = ['label' => 'Opentypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opentype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
