<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $order
 * @property string $client_name
 * @property string $email
 * @property string $phone
 * @property string $date
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order'], 'string'],
            [['client_name', 'phone'], 'required'],
            [['date'], 'safe'],
            [['client_name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order' => 'Order',
            'client_name' => 'Client Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'date' => 'Date',
        ];
    }
}
