<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "opentype".
 *
 * @property integer $id
 * @property string $type
 * @property integer $price
 */
class Opentype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opentype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'price'], 'required'],
            [['price'], 'integer'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'price' => 'Price',
        ];
    }
}
