<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "casements".
 *
 * @property integer $id
 * @property integer $oneflap
 * @property integer $twoflap
 * @property integer $treeflap
 * @property integer $framula
 */
class Casements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'casements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oneflap', 'twoflap', 'treeflap', 'framula'], 'required'],
            [['oneflap', 'twoflap', 'treeflap', 'framula'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oneflap' => 'Oneflap',
            'twoflap' => 'Twoflap',
            'treeflap' => 'Treeflap',
            'framula' => 'Framula',
        ];
    }
}
