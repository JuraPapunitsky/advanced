<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m160204_214611_create_table_order extends Migration
{
    public function up()
    {
        
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        
        
        $this->createTable('order', [
            'id' => Schema::TYPE_PK,
            'order' => Schema::TYPE_TEXT,
            'client_name'=>  Schema::TYPE_STRING.' NOT NULL',
            'email'=>Schema::TYPE_STRING,
            'phone'=>Schema::TYPE_STRING.' NOT NULL', 
            'date'=>  Schema::TYPE_TIMESTAMP,
        ]);
    }

    public function down()
    {
        $this->dropTable('order');
    }
}
