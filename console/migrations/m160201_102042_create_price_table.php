<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_102042_create_price_table extends Migration
{
   public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%size}}',
            [
                'id' => Schema::TYPE_PK,
                'sqare' => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%casements}}',
            [
                'id' => Schema::TYPE_PK,
                'oneflap' => Schema::TYPE_INTEGER . ' NOT NULL',
                'twoflap' => Schema::TYPE_INTEGER . ' NOT NULL',
                'treeflap' => Schema::TYPE_INTEGER . ' NOT NULL',
                'framula' => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%series}}',
            [
                'id' => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'price' => Schema::TYPE_INTEGER . ' NOT NULL',
                
            ],
            $tableOptions
        );
        
        $this->createTable(
            '{{%glazing}}',
            [
                'id' => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL',
                'price' => Schema::TYPE_INTEGER . ' NOT NULL',
                
            ],
            $tableOptions
        );
    }

    public function down()
    {
        $this->dropTable('{{%size}}');
        $this->dropTable('{{%casements}}');
        $this->dropTable('{{%series}}');
        $this->dropTable('{{%glazing}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
